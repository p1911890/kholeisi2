@extends('layouts.layout')

@section('titrePage')
    Les billets
@endsection


@section('contenu')
<body>
@foreach($billets as $b)
    <a href="{{ route('billets.show', $b->id) }}"><h2>{{ $b->BIL_Titre }}</h2></a>
    <a>{{ $b->BIL_Date}}</a><br>
    <a>{{ $b->BIL_Contenu}}</a><br>
    <br>
@endforeach
</body>
@endsection
