@extends('layouts.layout')

@section('titrePage')
    Commentaires
@endsection

@section('contenu')
    <div class="card">
        <header class="card-header">
            <h5 class="card-header-title">Titre: {{ $billets->BIL_Titre }}</h5>
        </header>
        <div class="card-content">
            <div class="content">
                <a>{{ $billets->BIL_Date}}</a><br>
                <a>{{ $billets->BIL_Contenu}}</a><br>
            </div>
        </div>
    </div>
@endsection